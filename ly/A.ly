%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  xA2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes xA16 \unHideNotes \bar "'"  %1
  
  s16 <xB xC>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  xD1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  xE1 \fermata  \bar "'"  %1
  s2. 
  s1
  xF4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  xG1 \startTrillSpan xA  ^\markup { \box { "5''" }}  \bar "'"  %1
  xA1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
