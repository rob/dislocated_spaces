#!/bin/bash

# import the configuration file containing
# the data from the acoustic analysis
source config.ini
##########################################

cd ly/

# i know i need to do some nesty looping action here
# but the concert starts in 2 hours.... quick and dirty it has to be!

cp A.ly A.tmp
C=0;
for i in xA xB xC xD xE xF xG xC xD xE xF xG
do
    sed -i  s"/$i/${pitchSet[$C]}/"g  A.tmp; 
    C=$[(C + 1)%${#pitchSet[@]}] 
done

C=0;
cp A.ly B.tmp
for i in xB xC xD xE xF xG xA xD xE xF xG xA
do
    sed -i s"/$i/${pitchSet[$C]}/"g  B.tmp; 
    C=$[(C + 1)%${#pitchSet[@]}] 
done
C=0;
cp A.ly C.tmp
for i in xC xD xE xF xG xA xB xE xF xG xA xB
do
    sed -i s"/$i/${pitchSet[$C]}/"g  C.tmp; 
    C=$[(C + 1)%${#pitchSet[@]}] 
done
C=0;
cp A.ly D.tmp
for i in xD xE xF xG xA xB xC xF xG xA xB xC
do
    sed -i s"/$i/${pitchSet[$C]}/"g  D.tmp; 
    C=$[(C + 1)%${#pitchSet[@]}] 
done
C=0;
cp A.ly E.tmp
for i in xE xF xG xA xB xC xD xG xA xB xC xD
do
    sed -i s"/$i/${pitchSet[$C]}/"g  E.tmp; 
    C=$[(C + 1)%${#pitchSet[@]}] 
done
C=0;
cp A.ly F.tmp
for i in xF xG xA xB xC xD xE
do
    sed -i s"/$i/${pitchSet[$C]}/"g  F.tmp; 
    C=$[(C + 1)%${#pitchSet[@]}] 
done
C=0;
cp A.ly G.tmp
for i in xG xA xB xC xD xE xF
do
    sed -i s"/$i/${pitchSet[$C]}/"g  G.tmp; 
    C=$[(C + 1)%${#pitchSet[@]}] 
done

cat A.tmp B.tmp C.tmp D.tmp E.tmp F.tmp   > tmp/RH.ly


###########################################################
cp B.ly BA.tmp

C=0;
for i in xA xB xC xD xE xF xG
do
    sed -i  s"/$i/${pitchSetB[$C]}/"g  BA.tmp; 
    C=$[(C + 1)%${#pitchSetB[@]}] 
done

C=0;
cp B.ly BB.tmp
for i in xB xC xD xE xF xG xA
do
    sed -i s"/$i/${pitchSetB[$C]}/"g  BB.tmp; 
        C=$[(C + 1)%${#pitchSetB[@]}] 
done
C=0;
cp B.ly BC.tmp
for i in xC xD xE xF xG xA xB
do
    sed -i s"/$i/${pitchSetB[$C]}/"g  BC.tmp; 
    C=$[(C + 1)%${#pitchSetB[@]}] 
done
C=0;
cp B.ly BD.tmp
for i in xD xE xF xG xA xB xC
do
    sed -i s"/$i/${pitchSetB[$C]}/"g  BD.tmp; 
    C=$[(C + 1)%${#pitchSetB[@]}] 
done
C=0;
cp B.ly BE.tmp
for i in xE xF xG xA xB xC xD
do
    sed -i s"/$i/${pitchSetB[$C]}/"g  BE.tmp; 
    C=$[(C + 1)%${#pitchSetB[@]}] 
done
C=0;
cp B.ly BF.tmp
for i in xF xG xA xB xC xD xE
do
    sed -i s"/$i/${pitchSetB[$C]}/"g  BF.tmp; 
    C=$[(C + 1)%${#pitchSetB[@]}] 
done

C=0;
cp B.ly BG.tmp
for i in xG xA xB xC xD xE xF
do
    sed -i s"/$i/${pitchSetB[$C]}/"g  BG.tmp; 
    C=$[(C + 1)%${#pitchSetB[@]}] 
done

cat BA.tmp BB.tmp BC.tmp BD.tmp BE.tmp BF.tmp BG.tmp BA.tmp   > tmp/LH.ly

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

C=0;
cp D.ly tmp/DRONE.ly
for i in "xA" "xB" "xC" "xD" "xE"
do
    sed -i s"/$i/${pitchSetD[$C]}/"g  tmp/DRONE.ly; 
    C=$[(C + 1)%${#pitchSetD[@]}] 
done

C=0;
cp R.ly tmp/RECORDER.ly
for i in "xA" "xB" "xC" "xD" "xE"
do
    sed -i s"/$i/${pitchSet[$C]}/"g  tmp/RECORDER.ly; 
    C=$[(C + 1)%${#pitchSet[@]}] 
done

C=0;
cp V.ly tmp/VOICE.ly
for i in "xA" "xB" "xC" "xD" "xE"
do
    sed -i s"/$i/${pitchSetV[$C]}/"g  tmp/VOICE.ly; 
    C=$[(C + 1)%${#pitchSetV[@]}] 
done


rm *.tmp
