
# three high---------------

version = "canterbury"
A=435

#pedal
#114.3hz # Bflat2 (-34)

#419.5hz # Aflat4 (+17)
#1154.7 # D6 (-30)

#1524.7 # G6 (-48)
#2585.6 # E7 (-34)
#3131.5 # G7 (-2)

#3620.9 # A7 (+49)
#3959.2 # B7 (+4)
#5020.4 # EFlat8 (+15)

#5507.5 # F8 (-25)
#5770.5 # GFlat (-44)
#7538.4
#8007.5
#9756.5
#11247.4
#11712.4

H1="ees" # C6+41
H2="b" # BFlat5+29c
H3="a" # AFLat5+22

# three mid ---------------
M1="g" # Fsharp5+5
M2="e" # Eflat5 - bingo 
M3="g" # Eflat5 - bingo 

#two low tomes ---------------
L1="d" # C5+22c
L2="aes" # 169.3hz - E3+46c
#---------------

# pedal tone G1-20c 48.4hz ---------------
P="bes"

#---------------
o_2 =",,"
o_1 =","
o0 = ""
o1 = "'"
o2 = "''"
o3 = "'''"

# RH
pitchSet=[L1+o1, L2+o1, M1+o2, M2+o3,  H1 + o2, H2 + o2,  H3 + o3]
# LH
pitchSetB=[ M1,  L1, M3+o_1, L2, H1+o1,  H2+o1, H3+o1]

# CHORD
pitchSetD=[ P+o_2,  H1+o_1, H2+o_1, M1+o1, M2]

# VOICE
pitchSetV=[ L1+o1, L2+o0, M2+o1, P+o0 ]

#textSet=( "aaaaaaaaaaa'" "ddddddddddddddd" "bbbbbbbbbb" "cccccccccccccc" )
