
version = "catford"

# organ tuning
A=442

#pedal
# 71.9hz  D  -36
# 155hz   Ef -2   !
# 270hz   C# +45 x
# 475hz   Bf +35
# 622hz   Ef +1   !

# 852 	  Af +45  !
# 1199	  D  +36  !
# 1828    Bf -34
# 2340    D  -7   !
# 2576    E  -40  !
# 2938    Gf -13
# 3870    B -36
# 4219    C  +14
# 4580    D  -44
# 5387    E  +37
# 5918    F#  0    !
# 6679    G  +9
# 6990    A  -12


#419.5hz # Aflat4 (+17)
#1154.7 # D6 (-30)

#1524.7 # G6 (-48)
#2585.6 # E7 (-34)
#3131.5 # G7 (-2)

#3620.9 # A7 (+49)
#3959.2 # B7 (+4)
#5020.4 # EFlat8 (+15)

#5507.5 # F8 (-25)
#5770.5 # GFlat (-44)
#7538.4
#8007.5
#9756.5
#11247.4
#11712.4

# three high---------------
H1="a" # C6+41
H2="g" # BFlat5+29c
H3="fis" # AFLat5+22

# three mid ---------------
M1="bes" # Fsharp5+5
M2="c" # Eflat5 - bingo 
M3="fis" # Eflat5 - bingo 

#two low tomes ---------------
L1="des" # C5+22c
L2="bes" # 169.3hz - E3+46c
#---------------

# pedal tone G1-20c 48.4hz ---------------
P="ees"

#---------------
o_2 =",,"
o_1 =","
o0 = ""
o1 = "'"
o2 = "''"
o3 = "'''"

# RH
pitchSet=[L1+o1, L2+o1, M1+o2, M2+o3,  H1 + o2, H2 + o2,  H3 + o3]
# LH
pitchSetB=[ M1,  L1, M3+o_1, L2, H1+o1,  H2+o1, H3+o1]

# CHORD
pitchSetD=[ P+o_1,  H1+o_1, H2+o_1, M1+o1, M2]

# VOICE
pitchSetV=[ L1+o1, L2+o0, M2+o1, P+o0 ]

#textSet=( "aaaaaaaaaaa'" "ddddddddddddddd" "bbbbbbbbbb" "cccccccccccccc" )
