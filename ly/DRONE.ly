drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <xA xB xC xD xE>2\laissezVibrer 
}