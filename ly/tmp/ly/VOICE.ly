%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
\proportionalStuff 
  
\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #2 

\change Staff = A

\override Slur #'color = #red
\override Glissando #'color = #red
\override Accidental #'color = #red
\override Script #'color = #red
\override TextScript #'color = #red
\override Tie #'color = #red
\override NoteHead #'color = #red

\override NoteHead #'style = #'xcircle
\override Glissando #'style = #'zigzag
\tieDashed
r4. c'4.-\markup{"black..."}\glissando\<  s4 
ees2..~-\markup{"cat...."}\>


%\hideNotes 
\override NoteHead #'style = #'cross
s1 ees8~-\markup{"rave--ins--bo--ourn"}  ees1~  s1
%\unHideNotes


\override NoteHead #'color = #blue
  \override Accidental #'color = #blue
  \override TextScript #'color = #blue
  \override Script #'color = #blue
\override NoteHead #'style = #'xcircle 

  c''1 \verylongfermata ^\markup{"(recorder)"}
\override Slur #'color = #red
\override Glissando #'color = #red
\override Accidental #'color = #red
\override Script #'color = #red
\override TextScript #'color = #red
\override Tie #'color = #red
\override NoteHead #'color = #red

\override Glissando #'style = #'normal
\override NoteHead #'style = #'xcircle
des'4-\markup{"||:excalibur:||"}\fermata\glissando(
\tieDotted 
bes'2.)~-\markup{"47..136...54...75..136..60"} 
\override NoteHead #'style = #'cross
\hideNotes bes~  \unHideNotes c''1^\markup{"Witch!"} s1
\override NoteHead #'style = #'xcircle
s1 ees'4.~-\markup{"1 - 7 - 1"} s1

\override NoteHead #'style = #'cross
s1 c'8~-\markup{"drown...in...the..ford"}  c'1~  s1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
\override NoteHead #'color = #blue
  \override Accidental #'color = #blue
  \override TextScript #'color = #blue
  \override Script #'color = #blue
  
  \override NoteHead #'style = #'xcircle 
  <bes' c''>1 \verylongfermata ^\markup{"(Recorder)"}
