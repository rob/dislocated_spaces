/////////////////////////////////////////
# Space Specific Score Generator
/////////////////////////////////////////
//// Rob Canning 2014 - rc@kiben.net
/////////////////////////////////////////

## The Score Generation

- the cookit.py is the file to run to generate the score
- the config.py file is where to define the pitches from the acoustic analyis of the space.

ly/current.ly is the where the main score boilerplate needs to be.
all the other .ly files are the individual "parts/voices" to be injected.

After this has happened cookit calls lilypond to output the final score.

These final scores are datetagged and moved to output/archive

---

## The Acoustic Analysis

The freq_resp directory contains a snapshot of the acoustic analysis and a text file where I note the frequencies available.

This analysis was made with the nice and opensource:

Jaaa-0.8.4 (C) 2004-2010 Fons Adriaensen  <fons@kokkinizita.net>

---

## Live Electronics

The live electronics are currently handled by PureData (pd-extended) and the patches and abstractions live in the liveelec directory.

---



