%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
fis2.\glissando\< \bar "'" 
ees1~\> \bar "'" 
\tieHalfDashed ees2~ \hideNotes ees2~   \bar "'" 
\tieHalfDashed ees2~   ees2~ \unHideNotes   \bar "'" 
e1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< c' bes'>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
aes'2.\glissando\< \bar "'" 
fis1~\> \bar "'" 
\tieHalfDashed fis2~ \hideNotes fis2~   \bar "'" 
\tieHalfDashed fis2~   fis2~ \unHideNotes   \bar "'" 
cis1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< e c'>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
bes'2.\glissando\< \bar "'" 
aes'1~\> \bar "'" 
\tieHalfDashed aes'2~ \hideNotes aes'2~   \bar "'" 
\tieHalfDashed aes'2~   aes'2~ \unHideNotes   \bar "'" 
ees1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< cis e>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
c'2.\glissando\< \bar "'" 
bes'1~\> \bar "'" 
\tieHalfDashed bes'2~ \hideNotes bes'2~   \bar "'" 
\tieHalfDashed bes'2~   bes'2~ \unHideNotes   \bar "'" 
fis1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< ees cis>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
e2.\glissando\< \bar "'" 
c'1~\> \bar "'" 
\tieHalfDashed c'2~ \hideNotes c'2~   \bar "'" 
\tieHalfDashed c'2~   c'2~ \unHideNotes   \bar "'" 
aes'1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< fis ees>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
cis2.\glissando\< \bar "'" 
e1~\> \bar "'" 
\tieHalfDashed e2~ \hideNotes e2~   \bar "'" 
\tieHalfDashed e2~   e2~ \unHideNotes   \bar "'" 
bes'1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< aes' fis>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
ees2.\glissando\< \bar "'" 
cis1~\> \bar "'" 
\tieHalfDashed cis2~ \hideNotes cis2~   \bar "'" 
\tieHalfDashed cis2~   cis2~ \unHideNotes   \bar "'" 
c'1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< bes' aes'>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
fis2.\glissando\< \bar "'" 
ees1~\> \bar "'" 
\tieHalfDashed ees2~ \hideNotes ees2~   \bar "'" 
\tieHalfDashed ees2~   ees2~ \unHideNotes   \bar "'" 
e1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< c' bes'>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
