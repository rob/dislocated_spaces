% LilyPond score file
\version "2.16.2"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Dislocated Spaces %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 2014 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Rob Canning %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% "Copyright Rob Canning 2014 %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% (Attribution-Non-Commercxial 2.0 UK %%%%%%%%%%%%%%%%  
%%%%%%%%%%%%%%%%%%%%%%%%% http://creativecommons.org %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% http://rob.kiben.net %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%modern = \accidentalStyle modern
%modernCautionary = \accidentalStyle modern-cautionary

proportionalStuff = {
%  \set suggestAccidentals = ##t
       \time 4/4 
       \override NoteHead #'style = #'neomensural
       \override Stem #'length = #0
       \override Score.Rest #'transparent = ##t 
       \override Score.Dots #'stencil = ##f
       \override Flag #'stencil = #no-flag
       \override Score.Stem #'transparent = ##t  
       \override Beam #'transparent = ##t
       \override Staff.BarLine #'transparent = ##f
       \override TupletNumber #'transparent = ##t
       \override TupletBracket #'transparent = ##t
       \override Staff.TimeSignature #'stencil = ##f
       \override Staff.TimeSignature #'transparent = ##t
       %\override Tie #'details #'ratio = #0.01
       %\override Tie #'details #'height-limit = #0
       %\override TextScript #'X-offset = #-16
       \override TextScript #'Y-offset = #0
       \override TextScript #'(font-name) = "Baskerville" 
       \override TextScript #'font-size = #-2
}

%%% THESE STRINGS GET REPLACED BY A SCRIPt
%%% WHICH INJECTS CONTENTS IN THEIR PLACE

drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <bes' c ees, a, g,>2\laissezVibrer 
}drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <ees, a, g, bes' c>2\laissezVibrer 
}drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <g, bes' c ees, a,>2\laissezVibrer 
}drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <c ees, a, g, bes'>2\laissezVibrer 
}drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <a, g, bes' c ees,>2\laissezVibrer 
}drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <bes' c ees, a, g,>2\laissezVibrer 
}drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <ees, a, g, bes' c>2\laissezVibrer 
}

addin = \new Voice { 
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
\proportionalStuff 
  
\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #2 

\change Staff = A

\override Slur #'color = #red
\override Glissando #'color = #red
\override Accidental #'color = #red
\override Script #'color = #red
\override TextScript #'color = #red
\override Tie #'color = #red
\override NoteHead #'color = #red

\override NoteHead #'style = #'xcircle
\override Glissando #'style = #'zigzag
\tieDashed
r4. c'4.-\markup{"black..."}\glissando\<  s4 
ees2..~-\markup{"cat...."}\>


%\hideNotes 
\override NoteHead #'style = #'cross
s1 ees8~-\markup{"rave--ins--bo--ourn"}  ees1~  s1
%\unHideNotes


\override NoteHead #'color = #blue
  \override Accidental #'color = #blue
  \override TextScript #'color = #blue
  \override Script #'color = #blue
\override NoteHead #'style = #'xcircle 

  c''1 \verylongfermata ^\markup{"(recorder)"}
\override Slur #'color = #red
\override Glissando #'color = #red
\override Accidental #'color = #red
\override Script #'color = #red
\override TextScript #'color = #red
\override Tie #'color = #red
\override NoteHead #'color = #red

\override Glissando #'style = #'normal
\override NoteHead #'style = #'xcircle
des'4-\markup{"||:excalibur:||"}\fermata\glissando(
\tieDotted 
bes'2.)~-\markup{"47..136...54...75..136..60"} 
\override NoteHead #'style = #'cross
\hideNotes bes~  \unHideNotes c''1^\markup{"Witch!"} s1
\override NoteHead #'style = #'xcircle
s1 ees'4.~-\markup{"1 - 7 - 1"} s1

\override NoteHead #'style = #'cross
s1 c'8~-\markup{"drown...in...the..ford"}  c'1~  s1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
\override NoteHead #'color = #blue
  \override Accidental #'color = #blue
  \override TextScript #'color = #blue
  \override Script #'color = #blue
  
  \override NoteHead #'style = #'xcircle 
  <bes' c''>1 \verylongfermata ^\markup{"(Recorder)"}


}

spacerVoice = \new Voice {
  \proportionalStuff 
  \override MultiMeasureRest #'minimum-length = #18
  \override MultiMeasureRest #'transparent = ##t
  R1 
}

markOrnIn = \once \override Script #'script-priority = #-100
textSpan = #(define-music-function (par loc dir str) (number? string?) #{\override TextSpanner #'direction = #$dir \override TextSpanner #'edge-text = #(cons $str ":") #})

noteHead = #(define-music-function (par loc sty) (symbol?) #{\once \override NoteHead #'style = #$sty #})

voiceA = {
  \set Staff.instrumentName = ""
  \set Staff.shortInstrumentName = ""
  \change Staff = A \override Staff.TimeSignature #'style = #'()
  \change Staff = B \override Staff.TimeSignature #'style = #'()
  \override Staff.TimeSignature #'stencil = ##f
  \override TupletNumber #'text = #tuplet-number::calc-fraction-text
  \override NoteHead #'duration-log = #5
  \override NoteHead #'style = #'neomensural
  \autoBeamOff

  #(set-accidental-style 'voice)
  
  \change Staff = A \clef treble
  \change Staff = B \clef bass
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  << {\time 4/4 
      
      \voiceOne \change Staff = A
      \override TextSpanner #'style = #'trill
      \override TrillSpanner #'bound-details #'left #'text = #'()
      
      \override Score.BarLine #'color = #black
      %\override Staff.BarLine #'hair-thickness = #4.75 
      \override NoteHead #'duration-log = #5
      \override Score.BarNumber #'Y-offset = #2 
      \override TextScript #'(font-name) = "Baskerville" 
      \override TextScript #'font-size = #-2


% RH.ly %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      \override NoteHead #'style = #'diamond
      \override NoteHead #'duration-log = #5

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  des'2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes des'16 \unHideNotes \bar "'"  %1
  
  s16 <bes' bes''>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  c'''1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  a''1 \fermata  \bar "'"  %1
  s2. 
  s1
  g''4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  fis'''1 \startTrillSpan des'  ^\markup { \box { "5''" }}  \bar "'"  %1
  des'1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  g''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes g''16 \unHideNotes \bar "'"  %1
  
  s16 <fis''' des'>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  bes'1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  bes''1 \fermata  \bar "'"  %1
  s2. 
  s1
  c'''4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  a''1 \startTrillSpan g''  ^\markup { \box { "5''" }}  \bar "'"  %1
  g''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  c'''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes c'''16 \unHideNotes \bar "'"  %1
  
  s16 <a'' g''>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  fis'''1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  des'1 \fermata  \bar "'"  %1
  s2. 
  s1
  bes'4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  bes''1 \startTrillSpan c'''  ^\markup { \box { "5''" }}  \bar "'"  %1
  c'''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  bes'2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes bes'16 \unHideNotes \bar "'"  %1
  
  s16 <bes'' c'''>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  a''1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  g''1 \fermata  \bar "'"  %1
  s2. 
  s1
  fis'''4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  des'1 \startTrillSpan bes'  ^\markup { \box { "5''" }}  \bar "'"  %1
  bes'1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  fis'''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes fis'''16 \unHideNotes \bar "'"  %1
  
  s16 <des' bes'>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  bes''1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  c'''1 \fermata  \bar "'"  %1
  s2. 
  s1
  a''4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  g''1 \startTrillSpan fis'''  ^\markup { \box { "5''" }}  \bar "'"  %1
  fis'''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  a''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes a''16 \unHideNotes \bar "'"  %1
  
  s16 <g'' fis'''>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  des'1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  bes'1 \fermata  \bar "'"  %1
  s2. 
  s1
  bes''4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  c'''1 \startTrillSpan a''  ^\markup { \box { "5''" }}  \bar "'"  %1
  a''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  bes''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes bes''16 \unHideNotes \bar "'"  %1
  
  s16 <c''' a''>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  g''1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  fis'''1 \fermata  \bar "'"  %1
  s2. 
  s1
  des'4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  bes'1 \startTrillSpan bes''  ^\markup { \box { "5''" }}  \bar "'"  %1
  bes''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

     } \\




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\set Staff.pedalSustainStyle = #'bracket
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     {        
       \proportionalStuff
       \change Staff = B 
       \override NoteHead #'style = #'diamond
       \override NoteHead #'duration-log = #5

       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
bes2.\glissando\< \bar "'" 
des1~\> \bar "'" 
\tieHalfDashed des2~ \hideNotes des2~   \bar "'" 
\tieHalfDashed des2~   des2~ \unHideNotes   \bar "'" 
bes1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< a' g'>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
g'2.\glissando\< \bar "'" 
fis'1~\> \bar "'" 
\tieHalfDashed fis'2~ \hideNotes fis'2~   \bar "'" 
\tieHalfDashed fis'2~   fis'2~ \unHideNotes   \bar "'" 
des1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< fis, bes>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
bes2.\glissando\< \bar "'" 
a'1~\> \bar "'" 
\tieHalfDashed a'2~ \hideNotes a'2~   \bar "'" 
\tieHalfDashed a'2~   a'2~ \unHideNotes   \bar "'" 
fis'1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< bes des>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
des2.\glissando\< \bar "'" 
fis,1~\> \bar "'" 
\tieHalfDashed fis,2~ \hideNotes fis,2~   \bar "'" 
\tieHalfDashed fis,2~   fis,2~ \unHideNotes   \bar "'" 
a'1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< g' fis'>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
fis'2.\glissando\< \bar "'" 
bes1~\> \bar "'" 
\tieHalfDashed bes2~ \hideNotes bes2~   \bar "'" 
\tieHalfDashed bes2~   bes2~ \unHideNotes   \bar "'" 
fis,1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< bes a'>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
a'2.\glissando\< \bar "'" 
g'1~\> \bar "'" 
\tieHalfDashed g'2~ \hideNotes g'2~   \bar "'" 
\tieHalfDashed g'2~   g'2~ \unHideNotes   \bar "'" 
bes1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< des fis,>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
fis,2.\glissando\< \bar "'" 
bes1~\> \bar "'" 
\tieHalfDashed bes2~ \hideNotes bes2~   \bar "'" 
\tieHalfDashed bes2~   bes2~ \unHideNotes   \bar "'" 
g'1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< fis' bes>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
bes2.\glissando\< \bar "'" 
des1~\> \bar "'" 
\tieHalfDashed des2~ \hideNotes des2~   \bar "'" 
\tieHalfDashed des2~   des2~ \unHideNotes   \bar "'" 
bes1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< a' g'>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
g'2.\glissando\< \bar "'" 
fis'1~\> \bar "'" 
\tieHalfDashed fis'2~ \hideNotes fis'2~   \bar "'" 
\tieHalfDashed fis'2~   fis'2~ \unHideNotes   \bar "'" 
des1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< fis, bes>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      


       s1 \bar ":|."
     }
  >>
}

voiceDrone = { \repeat unfold 58 {  \drone }}
voiceAddin = {   
  \repeat unfold 4 {  \addin }  
}

voiceAS = {
%  \skip 1*4/4*18
  #(set-accidental-style 'voice)
  \override NoteHead #'duration-log = #5
  \override NoteHead #'style = #'neomensural
  \repeat unfold 50 { \spacerVoice } 
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#(set-default-paper-size "a3" 'landscape)
#(set-global-staff-size 23)

\paper {
  %page-count = 2
  top-system-spacing #'minimum-distance = #22
  top-system-spacing #'space = #22
  last-bottom-spacing #'minimum-distance = #22
  last-bottom-spacing #'space = #22
  last-bottom-spacing #'padding = #2
  last-bottom-spacing #'stretchability = #2

  top-margin = 1\cm
  bottom-margin = 1\cm
  evenFooterMarkup = \markup \fill-line \override #'(font-name . "Optima") \fontsize #-2  {"Copyright Rob Canning 2014-2015 (Attribution-Non-Commercial 2.0 UK  http://creativecommons.org)"}
  oddFooterMarkup = \markup \fill-line \override #'(font-name . "Optima") \fontsize #-2 { " h t t p : / / r o b . k i b e n . n e t" }
  evenHeaderMarkup = \markup \fill-line { " [D i s] L o c a t e d  S p a c e [s] " }
  between-system-space = 1\cm
  ragged-bottom=##f
}

\header { 
  title = \markup \fontsize #3 {" [ D i s ]  L o c a t e d  S p a c e [s] "}
  composer =\markup    \override #'(font-name . "Optima") \fontsize #-1  {" R o b   C a n n i n g   2 0 1 4  "}
  subtitle = \markup \override #'(font-name . "Optima") \fontsize #1 {" f o r  O r g a n  a n d   E l e c t r o n i c s "}
  
  %subsubtitle = \markup \override #'(font-name . "Optima") \fontsize #-1 {"  "}
  subsubtitle = \markup \override #'(font-name . "Optima") \fontsize #-3 {" f o r  L a u r e n  R e d h e a d - Canterbury Version  "}
}

\score {
  \new PianoStaff <<
    #(set-accidental-style 'voice)
    \context Staff = A \new Voice \voiceAS 
    \context Staff = B \new Voice \voiceAS
    \context Staff = A \new Voice \voiceA
    \context Staff = B  \new Voice \voiceDrone 
    \context Staff = A  \new Voice \voiceAddin   
  >>
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%