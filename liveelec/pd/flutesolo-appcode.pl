#!/usr/bin/perl

#use strict;
use warnings;

#load module
use Math::Combinatorics;

my $filename = "5forflute";
$pwd = `pwd`;
chomp ($pwd);
$dte = `date --iso`;
chomp ($dte);

#definition of 5 elements for permutation

my @frag1 = qw(a'' bes'' e'');
my @frag2 = qw( b'');
my @frag3 = qw(des'' c'');

my @n = ([@frag1],  [@frag2],  ["fis''"],  ["g''"], [@frag3]); 
#my @n = (["a''"],  ["b''"],  ["fis''"],  ["g''"], ["d''"]); 

#do permutations of 5! ###################################### 
my  @perm = map {@$_} permute(@n);

#flatten permutation array####################################

my @flatperm = map { @$_ } @perm;
print  @flatperm;

# get date for score datestamp
my $date;
$date = localtime(time);

#looping subroutines that puts array data into sprintf

sub InsertPitches {
   my ($num, $form, @notes) = @_;
   my ($output, @temp);
 
  while (@notes) {
     (@temp[0..$num-1], @notes) = @notes;

     $output .= sprintf $form, (@temp);
   }

   return $output;
}

sub InsertDurations { 
  my ($num, $form, @notes) = @_;
   my ($output, @temp);
 
  while (@notes) {
     (@temp[0..$num-1], @notes) = @notes;

     $output .= sprintf $form, (@temp);
   }

 #  return $output;
# my @test = &InsertDurations;
}
#definition of phrase format - with pitch / drum information as %1\$ variable
#################################################################

#\\time 4/4 %32\$s16 %33\$s16 %34\$s16. %35\$s32 \\times 2/3 { %36\$s16-> %37\$s8 %38\$s16 %39\$s2 \\mordent } r4\\shortfermata |
#\\time 4/2%31\$s\\breve \\espressivo \\breathe |
#\\time 4/4 %55\$s16-> %56\$s16 %57\$s16 %58\$s16-> %59\$s16 %60\$s16 %60\$s16 %59\$s16 r2 \\verylongfermata |
#\\time 7/4 \\times 4/5 { %40\$s16-> %41\$s16 %42\$s16 %43\$s16 %44\$s16 %45\$s8-> %46\$s16 %47\$s4 
#\\times 2/3 { %48\$s8  %49\$s8-> %50\$s16 %51\$s16 } r4\\fermata } %52\$s8  %53\$s16-. %54\$s16-> r2 \\longfermata \\stopGroup |


my $formatA = "

\\override HorizontalBracket #'direction = #UP

\\time 4/2 %1\$s\\breve\\startGroup \\espressivo \\breathe |
\\time 4/4 %2\$s16 %3\$s16 %4\$s16. %5\$s32 \\times 2/3 { %6\$s16-> %7\$s8 %8\$s16 %9\$s2 \\mordent } r4\\shortfermata |
\\time 7/4 \\times 4/5 { %10\$s16-> %11\$s16 %12\$s16 %13\$s16 %14\$s16 %15\$s8-> %16\$s16 %17\$s4 
\\times 2/3 { %18\$s8  %19\$s8-> %20\$s16 %21\$s16 } r4\\fermata } %22\$s8  %23\$s16-. %24\$s16-> r2 \\longfermata |
\\time 4/2 %1\$s\\breve \\espressivo\\trill \\breathe |
\\time 4/4 %25\$s16-> %26\$s16 %27\$s16 %28\$s16-> %29\$s16 %30\$s16 %29\$s16 %30\$s16 r2 \\verylongfermata |

\\time 4/2 %31\$s\\breve  \\breathe |
\\time 7/4 \\times 4/5 { %36\$s16-> %36\$s16 %34\$s16 %35\$s16 %36\$s16 %34\$s8-> %35\$s16 %36\$s4 
\\times 2/3 { %37\$s8  %38\$s8-> %39\$s16 %21\$s16 } r4\\fermata } %40\$s8  %40\$s16-. %40\$s16-> r2 \\longfermata |
\\time 4/4 %31\$s16 %31\$s16 %31\$s16. %32\$s32 \\times 2/3 { %32\$s16-> %33\$s8 %34\$s16 %35\$s2 \\trill } r4\\shortfermata |
\\time 4/4 %40\$s16-> %40\$s16 %41\$s16 %42\$s16-> %43\$s16 %44\$s16 %45\$s16 %45\$s16~ %45\$s2 \\verylongfermata \\stopGroup|

 \n ";

#header information for score
my @score = ("

\\version \"2.12.2\" \n
%\\include \"$pwd/aux/titlepage.ly\" \n
%\\score {
\#(set-default-paper-size \"a4\")
\#(set-global-staff-size 19)
\\paper {
  page-count = #3
 ragged-bottom = ##f
 ragged-last-bottom = ##f
 top-margin = 35\cm  
 bottom-margin = 35\cm  
% left-margin = 15\cm 
% line-width = 385\cm

%system-count = 6
}

\\header {
  title = \\markup \\bold \\italic \\fontsize #9  \"5!\"
  subtitle = \"for flutes\"
  subsubtitle = \"\"
  poet = \"2009\"

  composer =   \"Rob Canning\"
  copyright = \\markup \\fontsize #-4 \"\" 
  tagline = \\markup \\fontsize #-4 \"\"
}


\\score {
\\new Staff \\with {
    %\\consists Horizontal_bracket_engraver
} {

  \\override Staff.BarLine #'extra-offset = #'(0 . 2)
   \\tempo 4 = 92 
  
     \n ", "{\n", 

#insert flat data into score
InsertPitches(120, $formatA, @flatperm ), "\n}}",

#generate midi file

" \\layout {}
  \\midi{\\context {  
      \\Staff}}}"
, "\n\n\n\n"
);

#open and print to file
my $filename = "5forFlute";
system("mkdir /tmp/$filename");
open SCOREOUT, ">/tmp/flute/$filename.ly";
print SCOREOUT @score;

# parse score and print view pdf                                                                                                      

my $runlily = system("mkdir /tmp/flute; 
cp $pwd/aux/titlepage.ly /tmp/flute;  
cd /tmp/flute; 
lilypond --pdf -o $filename /tmp/flute/$filename");
my $pdftk = system("pdftk titlepages.pdf /tmp/flute/$filename.pdf cat output /tmp/flute/$filename-score.pdf");
my $runxpdf = system("evince /tmp/flute/$filename-score.pdf &");
my $timidity = system("timidity /tmp/flute/$filename.midi -Ow -o  /tmp/flute/$filename.wav &");
my $timidity = system("timidity /tmp/flute/$filename.midi&");

unlink ("/tmp/flute/$filename.ps");
