#!/usr/bin/python

import shutil, re, glob, os, random, time

#import the config file containing acoustic analysis data
cfg=__import__("config")

# set lilypond file path
l="ly"

# define the part names 
parts=["LH","RH","DRONE","VOICE", "RECORDER"]

# define the number of slices (template variants/rotations) that make a part
variants=["A","B","C","D","E","F","G","A","B","C","D","E","F","G"]
# define the search keys (these strings are in lilypond templates and to be replaced):
col =  ["xA", "xB", "xC", "xD", "xE", "xF", "xG", "xC", "xD", "xE", "xF", "xG"]

# ---------------------------------------
# clear the old tmp files before starting
deleteme = ["ly/tmp/*.tmp", "*.pdf", "ly/located*.ly", "ly/tmp/ly/*.ly"]
for d in deleteme:
  for clean in glob.glob(d):
    os.remove(clean)
# ---------------------------------------

C=0; # a counter to use for rota

for idz, p in enumerate(parts):
    files2cat = []
    # decide which pitchset to use based on part
    if p == "LH":
      pitches = cfg.pitchSet
      varinums = 7;
    elif p is "RH":
      pitches = cfg.pitchSetB
      varinums = 9;
    elif p is "DRONE":
      pitches = cfg.pitchSetD
      varinums = 7;
    elif p is "VOICE":
      pitches = cfg.pitchSetV
      varinums = 1;
    elif p is "RECORDER":
      pitches = cfg.pitchSetV
      varinums = 1;
    else:
      pitches = cfg.pitchSetD
      varinums = 2;

    # variants[0:varinums] select the number of variants to use as tweaked in the elif above
    for idy, a in enumerate(variants[0:varinums]):
        # open lilypond input template file for search and replace
        f = open(l+"/"+p+".ly",'r')
        # and read its contents to a variable called filedata
        filedata = f.read()
        f.close()
        # takes last list element and moves it to the head of the list
        col.insert(0, col.pop(0))    
        # random.shuffle(col)
        print "populating template.......: " + p,a
        
        for idx, c in enumerate(col):
            f = open(l+"/tmp/" + p + a + ".tmp",'w')
            x = pitches[C%len(pitches)]
            # do the search and replace
            filedata = filedata.replace(c,x)
            # step through the pitches in the set to populate template
            # if the pitch set is shorter than the template use the modulus
            C=(C+1)%len(pitches);
            # open a new file for output
            f.write(filedata)
            f.close()
  
            # add the p 
        fstrg = 'ly/tmp/ly/' + p+'.ly'
        # append filenames to an array to be looped through below
        files2cat.append("ly/tmp/"+p+a+".tmp")
        print "concatenating template rotations to output file......:" + fstrg

    # open an output file
    with open(fstrg, 'a') as outfile:
        # loop through array of filenames and concatenate contents out fstrg output file
        for fc in files2cat:
            print fc
            with open(fc) as infile:
                for line in infile:
                    outfile.write(line)

#with open(, 'w') as outfile:

# take the master template and inject in the populated template parts

# prepend opening bars here:

print "============================================="

"""
with file('ly/tmp/ly/LH.ly', 'r') as original: data = original.read()
with file('ly/tmp/ly/LH.ly', 'w') as modified: modified.write('r1 \\bar ":" %%XXXXXXX' + data)


with file('ly/tmp/ly/RH.ly', 'r') as original: data = original.read()
with file('ly/tmp/ly/RH.ly', 'w') as modified: modified.write('r1 \\bar ":" %%XXXXXXX' + data)


with file('ly/tmp/ly/DRONE.ly', 'r') as original: data = original.read()
with file('ly/tmp/ly/DRONE.ly', 'w') as modified: modified.write('r1 \\bar ":" %%XXXXXXX' + data)

with file('ly/tmp/ly/RECORDER.ly', 'r') as original: data = original.read()
with file('ly/tmp/ly/RECORDER.ly', 'w') as modified: modified.write('r1 \\bar ":" %%XXXXXXX' + data)

#with file('ly/tmp/ly/VOICE.ly', 'r') as original: data = original.read()
#with file('ly/tmp/ly/VOICE.ly', 'w') as modified: modified.write('r1 \\bar ":" %%XXXXXXX' + data)
"""

lh = open('ly/tmp/ly/LH.ly').read()
rh = open('ly/tmp/ly/RH.ly').read()
drone = open('ly/tmp/ly/DRONE.ly').read()
voice = open('ly/tmp/ly/VOICE.ly').read()
recorder = open('ly/tmp/ly/RECORDER.ly').read()


file = open('ly/current.ly')
contents = file.read()

replaced_contents = contents.replace('AAAAA', lh)
replaced_contents = replaced_contents.replace('BBBBB', rh)
replaced_contents = replaced_contents.replace('ADDIN', voice)
replaced_contents = replaced_contents.replace('DRONE', drone)
replaced_contents = replaced_contents.replace('RECORDER', recorder)

timestr = time.strftime("%Y%m%d-%H%M")

fname = 'locatedspace_'+cfg.version+timestr
masterfile = open('ly/'+fname+'.ly', 'w')
masterfile.write(replaced_contents)
masterfile.close()

os.system('lilypond ' + 'ly/'+fname+'.ly')
os.system('evince ' +         fname + '.pdf')
