#!/usr/bin/perl -w

my $f1 = `cat ly/tmp/RH.ly`;
my $f2 = `cat ly/tmp/LH.ly`;
my $f3 = `cat ly/tmp/VOICE.ly`;
my $f4 = `cat ly/tmp/DRONE.ly`;
my $f5 = `cat ly/tmp/RECORDER.ly`;

while (<>) {
    chomp;
    s/AAAAA/$f1/;
    s/BBBBB/$f2/;
    s/ADDIN/$f3/;
    s/DRONE/$f4/;
    s/RECORDER/$f5/;
    print "$_\n";
}
