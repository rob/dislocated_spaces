%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
xA2.\glissando\< \bar "'" 
xB1~\> \bar "'" 
\tieHalfDashed xB2~ \hideNotes xB2~   \bar "'" 
\tieHalfDashed xB2~   xB2~ \unHideNotes   \bar "'" 
xD1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< xE xF>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
