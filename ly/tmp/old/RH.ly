%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  cis'2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes cis'16 \unHideNotes \bar "'"  %1
  
  s16 <e' fis''>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  ees'''1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  c''1 \fermata  \bar "'"  %1
  s2. 
  s1
  bes''4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  aes''1 \startTrillSpan cis'  ^\markup { \box { "5''" }}  \bar "'"  %1
  cis'1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  aes''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes aes''16 \unHideNotes \bar "'"  %1
  
  s16 <cis' e'>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  fis''1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  ees'''1 \fermata  \bar "'"  %1
  s2. 
  s1
  c''4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  bes''1 \startTrillSpan aes''  ^\markup { \box { "5''" }}  \bar "'"  %1
  aes''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  bes''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes bes''16 \unHideNotes \bar "'"  %1
  
  s16 <aes'' cis'>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  e'1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  fis''1 \fermata  \bar "'"  %1
  s2. 
  s1
  ees'''4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  c''1 \startTrillSpan bes''  ^\markup { \box { "5''" }}  \bar "'"  %1
  bes''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  c''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes c''16 \unHideNotes \bar "'"  %1
  
  s16 <bes'' aes''>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  cis'1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  e'1 \fermata  \bar "'"  %1
  s2. 
  s1
  fis''4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  ees'''1 \startTrillSpan c''  ^\markup { \box { "5''" }}  \bar "'"  %1
  c''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  ees'''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes ees'''16 \unHideNotes \bar "'"  %1
  
  s16 <c'' bes''>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  aes''1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  cis'1 \fermata  \bar "'"  %1
  s2. 
  s1
  e'4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  fis''1 \startTrillSpan ees'''  ^\markup { \box { "5''" }}  \bar "'"  %1
  ees'''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  fis''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes fis''16 \unHideNotes \bar "'"  %1
  
  s16 <ees''' c''>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  bes''1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  aes''1 \fermata  \bar "'"  %1
  s2. 
  s1
  cis'4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  e'1 \startTrillSpan fis''  ^\markup { \box { "5''" }}  \bar "'"  %1
  fis''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
