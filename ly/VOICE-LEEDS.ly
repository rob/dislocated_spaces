%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
\proportionalStuff 
  
\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #2 

\change Staff = A

\override Slur #'color = #red
\override Glissando #'color = #red
\override Accidental #'color = #red
\override Script #'color = #red
\override TextScript #'color = #red
\override Tie #'color = #red
\override NoteHead #'color = #red

\override NoteHead #'style = #'xcircle
\override Glissando #'style = #'zigzag
\tieDashed
r4. xA4.-\markup{"Loi..."}\glissando\<  s4 
xB2..~-\markup{"di--s..."}\>

%\hideNotes 
\override NoteHead #'style = #'cross
s1 xB8~-\markup{"sssss...."}  xB1~  s1
%\unHideNotes

\override NoteHead #'color = #blue
  \override Accidental #'color = #blue
  \override TextScript #'color = #blue
  \override Script #'color = #blue
\override NoteHead #'style = #'xcircle 

  xA'1 \verylongfermata ^\markup{"(recorder)"}
\override Slur #'color = #red
\override Glissando #'color = #red
\override Accidental #'color = #red
\override Script #'color = #red
\override TextScript #'color = #red
\override Tie #'color = #red
\override NoteHead #'color = #red

\override Glissando #'style = #'normal
\override NoteHead #'style = #'xcircle
xC4-\markup{"chau..."}\fermata\glissando(
\tieDotted 
xD'2.)~-\markup{"...ss..errrr!"} 
\override NoteHead #'style = #'cross
\hideNotes xD~  \unHideNotes xA'1^\markup{"kent!"} s1
\override NoteHead #'style = #'xcircle
s1 xB'4.~-\markup{"w- ooooo - l"} s1

\override NoteHead #'style = #'cross
s1 xE8~-\markup{"shhhhhh...."}  xE1~  s1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
\override NoteHead #'color = #blue
  \override Accidental #'color = #blue
  \override TextScript #'color = #blue
  \override Script #'color = #blue
  
  \override NoteHead #'style = #'xcircle 
  <xD' xA'>1 \verylongfermata ^\markup{"(Recorder)"}
