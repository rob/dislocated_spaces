\version "2.12.2"

#(set-global-staff-size 35)
#(define-markup-command (vspace layout props amount) (number?)
  "This produces a invisible object taking vertical space."
    (ly:make-stencil "" (cons -1 1) (cons 0 amount))
    (ly:make-stencil "" (cons -1 1) (cons amount amount)))


titlePageMarkup = \markup \abs-fontsize #10 \column {
    \vspace #54

    \fill-line {  \override #'(font-name . "Optima") \fontsize #32 \bold \fromproperty #'header:title }
    \vspace #9

    \fontsize #10 \fill-line { \override #'(font-name . "Optima") \fromproperty #'header:subtitle }
    \vspace #8

%    \fontsize #7 \fill-line { \fromproperty #'header:subsubtitle }
%    \vspace #56

    \fill-line { \override #'(font-name . "Optima")  \fontsize #7 \fromproperty #'header:composer }
    \vspace #24

%    \fill-line { \postscript #"-20 0 moveto 40 0 rlineto stroke" }
%    \vspace #48

}

\paper {

  bookTitleMarkup = \titlePageMarkup
}

\pageBreak
