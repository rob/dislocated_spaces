
# three high---------------

version = "canterbury"
A=435

#pedal
#114.3hz # Bflat2 (-34)

#419.5hz # Aflat4 (+17)
#1154.7 # D6 (-30)
#1524.7 # G6 (-48)
#2585.6 # E7 (-34)
#3131.5 # G7 (-2)
#3620.9 # A7 (+49)
#3959.2 # B7 (+4)
#5020.4 # EFlat8 (+15)
#5507.5 # F8 (-25)
#5770.5 # GFlat (-44)
#7538.4
#8007.5
#9756.5
#11247.4
#11712.4

H1="c" # C6+41
H2="bes" # BFlat5+29c
H3="aes" # AFLat5+22

# three mid ---------------
M1="fis" # Fsharp5+5
M2="ees" # Eflat5 - bingo 

#two low tomes ---------------
L1="cis" # C5+22c
L2="e" # 169.3hz - E3+46c
#---------------

# pedal tone G1-20c 48.4hz ---------------
P="g"
#---------------

o_1 =","
o0 = ""
o1 = "'"
o2 = "''"
o3 = "'''"


# RH
pitchSet=[L1 + "'", L2 + "'", M1 + o2, M2 + "'''",  H1 + "''", H2 + "''",  H3 + "''"]
# LH
pitchSetB=[ M1,  M2, L1, L2, H1+"'",  H2+"'", H3+"'"]

# CHORD
pitchSetD=[ P+o_1,  H1, H2, M1+"'", M2+"''"]

# VOICE
pitchSetV=[ L1+"'", L2+"'", M1+"''", H3 ]

#textSet=( "aaaaaaaaaaa'" "ddddddddddddddd" "bbbbbbbbbb" "cccccccccccccc" )
