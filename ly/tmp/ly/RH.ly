%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
bes2.\glissando\< \bar "'" 
des1~\> \bar "'" 
\tieHalfDashed des2~ \hideNotes des2~   \bar "'" 
\tieHalfDashed des2~   des2~ \unHideNotes   \bar "'" 
bes1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< a' g'>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
g'2.\glissando\< \bar "'" 
fis'1~\> \bar "'" 
\tieHalfDashed fis'2~ \hideNotes fis'2~   \bar "'" 
\tieHalfDashed fis'2~   fis'2~ \unHideNotes   \bar "'" 
des1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< fis, bes>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
bes2.\glissando\< \bar "'" 
a'1~\> \bar "'" 
\tieHalfDashed a'2~ \hideNotes a'2~   \bar "'" 
\tieHalfDashed a'2~   a'2~ \unHideNotes   \bar "'" 
fis'1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< bes des>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
des2.\glissando\< \bar "'" 
fis,1~\> \bar "'" 
\tieHalfDashed fis,2~ \hideNotes fis,2~   \bar "'" 
\tieHalfDashed fis,2~   fis,2~ \unHideNotes   \bar "'" 
a'1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< g' fis'>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
fis'2.\glissando\< \bar "'" 
bes1~\> \bar "'" 
\tieHalfDashed bes2~ \hideNotes bes2~   \bar "'" 
\tieHalfDashed bes2~   bes2~ \unHideNotes   \bar "'" 
fis,1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< bes a'>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
a'2.\glissando\< \bar "'" 
g'1~\> \bar "'" 
\tieHalfDashed g'2~ \hideNotes g'2~   \bar "'" 
\tieHalfDashed g'2~   g'2~ \unHideNotes   \bar "'" 
bes1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< des fis,>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
fis,2.\glissando\< \bar "'" 
bes1~\> \bar "'" 
\tieHalfDashed bes2~ \hideNotes bes2~   \bar "'" 
\tieHalfDashed bes2~   bes2~ \unHideNotes   \bar "'" 
g'1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< fis' bes>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
bes2.\glissando\< \bar "'" 
des1~\> \bar "'" 
\tieHalfDashed des2~ \hideNotes des2~   \bar "'" 
\tieHalfDashed des2~   des2~ \unHideNotes   \bar "'" 
bes1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< a' g'>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r4
\override Glissando #'style = #'zigzag
\override Glissando #'color = #'green
g'2.\glissando\< \bar "'" 
fis'1~\> \bar "'" 
\tieHalfDashed fis'2~ \hideNotes fis'2~   \bar "'" 
\tieHalfDashed fis'2~   fis'2~ \unHideNotes   \bar "'" 
des1 \bar "'" 
r1 \bar "'" 

%\override Staff.BarLine #'color = #red
< fis, bes>1  \bar "||"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
