%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
\proportionalStuff 
  
\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #2 

\change Staff = A

\override Slur #'color = #red
\override Glissando #'color = #red
\override Accidental #'color = #red
\override Script #'color = #red
\override TextScript #'color = #red
\override Tie #'color = #red
\override NoteHead #'color = #red

\override NoteHead #'style = #'xcircle
\override Glissando #'style = #'zigzag
\tieDashed
r4. cis'4.-\markup{"Loi..."}\glissando\<  s4 
e'2..~-\markup{"di--s..."}\>


%\hideNotes 
\override NoteHead #'style = #'cross
s1 e'8~-\markup{"sssss...."}  e'1~  s1
%\unHideNotes


\override NoteHead #'color = #blue
  \override Accidental #'color = #blue
  \override TextScript #'color = #blue
  \override Script #'color = #blue
\override NoteHead #'style = #'xcircle 

  cis''1 \verylongfermata ^\markup{"(recorder)"}
\override Slur #'color = #red
\override Glissando #'color = #red
\override Accidental #'color = #red
\override Script #'color = #red
\override TextScript #'color = #red
\override Tie #'color = #red
\override NoteHead #'color = #red

\override Glissando #'style = #'normal
\override NoteHead #'style = #'xcircle
fis''4-\markup{"e..."}\fermata\glissando(
\tieDotted 
aes'2.)~-\markup{"...l...met!"} 
\override NoteHead #'style = #'cross
\hideNotes aes~  \unHideNotes cis''1^\markup{"flax!"} s1
\override NoteHead #'style = #'xcircle
s1 e''4.~-\markup{"w- ooooo - l"} s1

\override NoteHead #'style = #'cross
s1 cis'8~-\markup{"shhhhhh...."}  cis'1~  s1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
\override NoteHead #'color = #blue
  \override Accidental #'color = #blue
  \override TextScript #'color = #blue
  \override Script #'color = #blue
  
  \override NoteHead #'style = #'xcircle 
  <aes' cis''>1 \verylongfermata ^\markup{"(Recorder)"}
