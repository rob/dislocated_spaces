#!/bin/bash


RENDERDATE=`date +"%Y%m%d-%H%M"`

# this does search and replace in all the extra files A.ly B.ly etc.
# and replaces xA xB etc with pitches from audio analysis
./construct.sh

# this takes the component files that now have pitches injected 
# into the placeholders and inserts them into the main current.ly placeholder
./magic.pl ly/current.ly > ly/tmp/out.ly

# renders the result into a pdf
lilypond -o ly/tmp/  ly/tmp/out.ly 

# backup version to archive
mv ly/tmp/out.pdf output/archive/dislocatedspaces-$RENDERDATE.pdf
mv ly/tmp/out.ly output/archive/dislocatedspaces-$RENDERDATE.ly

evince output/archive/dislocatedspaces-$RENDERDATE.pdf
