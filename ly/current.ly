% LilyPond score file
\version "2.16.2"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Dislocated Spaces %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 2014 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Rob Canning %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% "Copyright Rob Canning 2014 %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% (Attribution-Non-Commercxial 2.0 UK %%%%%%%%%%%%%%%%  
%%%%%%%%%%%%%%%%%%%%%%%%% http://creativecommons.org %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% http://rob.kiben.net %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%modern = \accidentalStyle modern
%modernCautionary = \accidentalStyle modern-cautionary

proportionalStuff = {
%  \set suggestAccidentals = ##t
       \time 4/4 
       \override NoteHead #'style = #'neomensural
       \override Stem #'length = #0
       \override Score.Rest #'transparent = ##t 
       \override Score.Dots #'stencil = ##f
       \override Flag #'stencil = #no-flag
       \override Score.Stem #'transparent = ##t  
       \override Beam #'transparent = ##t
       \override Staff.BarLine #'transparent = ##f
       \override TupletNumber #'transparent = ##t
       \override TupletBracket #'transparent = ##t
       \override Staff.TimeSignature #'stencil = ##f
       \override Staff.TimeSignature #'transparent = ##t
       %\override Tie #'details #'ratio = #0.01
       %\override Tie #'details #'height-limit = #0
       %\override TextScript #'X-offset = #-16
       \override TextScript #'Y-offset = #0
       \override TextScript #'(font-name) = "Baskerville" 
       \override TextScript #'font-size = #-2
}

%%% THESE STRINGS GET REPLACED BY A SCRIPt
%%% WHICH INJECTS CONTENTS IN THEIR PLACE

DRONE

addin = \new Voice { 
  
ADDIN

}

spacerVoice = \new Voice {
  \proportionalStuff 
  \override MultiMeasureRest #'minimum-length = #18
  \override MultiMeasureRest #'transparent = ##t
  R1 
}

markOrnIn = \once \override Script #'script-priority = #-100
textSpan = #(define-music-function (par loc dir str) (number? string?) #{\override TextSpanner #'direction = #$dir \override TextSpanner #'edge-text = #(cons $str ":") #})

noteHead = #(define-music-function (par loc sty) (symbol?) #{\once \override NoteHead #'style = #$sty #})

voiceA = {
  \set Staff.instrumentName = ""
  \set Staff.shortInstrumentName = ""
  \change Staff = A \override Staff.TimeSignature #'style = #'()
  \change Staff = B \override Staff.TimeSignature #'style = #'()
  \override Staff.TimeSignature #'stencil = ##f
  \override TupletNumber #'text = #tuplet-number::calc-fraction-text
  \override NoteHead #'duration-log = #5
  \override NoteHead #'style = #'neomensural
  \autoBeamOff

  #(set-accidental-style 'voice)
  
  \change Staff = A \clef treble
  \change Staff = B \clef bass
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  << {\time 4/4 
      
      \voiceOne \change Staff = A
      \override TextSpanner #'style = #'trill
      \override TrillSpanner #'bound-details #'left #'text = #'()
      
      \override Score.BarLine #'color = #black
      %\override Staff.BarLine #'hair-thickness = #4.75 
      \override NoteHead #'duration-log = #5
      \override Score.BarNumber #'Y-offset = #2 
      \override TextScript #'(font-name) = "Baskerville" 
      \override TextScript #'font-size = #-2


% RH.ly %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      \override NoteHead #'style = #'diamond
      \override NoteHead #'duration-log = #5

      AAAAA

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

     } \\




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\set Staff.pedalSustainStyle = #'bracket
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     {        
       \proportionalStuff
       \change Staff = B 
       \override NoteHead #'style = #'diamond
       \override NoteHead #'duration-log = #5

       BBBBB

       s1 \bar ":|."
     }
  >>
}

voiceDrone = { \repeat unfold 58 {  \drone }}
voiceAddin = {   
  \repeat unfold 4 {  \addin }  
}

voiceAS = {
%  \skip 1*4/4*18
  #(set-accidental-style 'voice)
  \override NoteHead #'duration-log = #5
  \override NoteHead #'style = #'neomensural
  \repeat unfold 50 { \spacerVoice } 
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#(set-default-paper-size "a3" 'landscape)
#(set-global-staff-size 23)

\paper {
  %page-count = 2
  top-system-spacing #'minimum-distance = #22
  top-system-spacing #'space = #22
  last-bottom-spacing #'minimum-distance = #22
  last-bottom-spacing #'space = #22
  last-bottom-spacing #'padding = #2
  last-bottom-spacing #'stretchability = #2

  top-margin = 1\cm
  bottom-margin = 1\cm
  evenFooterMarkup = \markup \fill-line \override #'(font-name . "Optima") \fontsize #-2  {"Copyright Rob Canning 2014-2015 (Attribution-Non-Commercial 2.0 UK  http://creativecommons.org)"}
  oddFooterMarkup = \markup \fill-line \override #'(font-name . "Optima") \fontsize #-2 { " h t t p : / / r o b . k i b e n . n e t" }
  evenHeaderMarkup = \markup \fill-line { " [D i s] L o c a t e d  S p a c e [s] " }
  between-system-space = 1\cm
  ragged-bottom=##f
}

\header { 
  title = \markup \fontsize #3 {" [ D i s ]  L o c a t e d  S p a c e [s] "}
  composer =\markup    \override #'(font-name . "Optima") \fontsize #-1  {" R o b   C a n n i n g   2 0 1 4  "}
  subtitle = \markup \override #'(font-name . "Optima") \fontsize #1 {" f o r  O r g a n  a n d   E l e c t r o n i c s "}
  
  %subsubtitle = \markup \override #'(font-name . "Optima") \fontsize #-1 {"  "}
  subsubtitle = \markup \override #'(font-name . "Optima") \fontsize #-3 {" f o r  L a u r e n  R e d h e a d - Canterbury Version  "}
}

\score {
  \new PianoStaff <<
    #(set-accidental-style 'voice)
    \context Staff = A \new Voice \voiceAS 
    \context Staff = B \new Voice \voiceAS
    \context Staff = A \new Voice \voiceA
    \context Staff = B  \new Voice \voiceDrone 
    \context Staff = A  \new Voice \voiceAddin   
  >>
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%