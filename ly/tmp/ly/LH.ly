%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  des'2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes des'16 \unHideNotes \bar "'"  %1
  
  s16 <bes' bes''>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  c'''1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  a''1 \fermata  \bar "'"  %1
  s2. 
  s1
  g''4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  fis'''1 \startTrillSpan des'  ^\markup { \box { "5''" }}  \bar "'"  %1
  des'1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  g''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes g''16 \unHideNotes \bar "'"  %1
  
  s16 <fis''' des'>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  bes'1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  bes''1 \fermata  \bar "'"  %1
  s2. 
  s1
  c'''4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  a''1 \startTrillSpan g''  ^\markup { \box { "5''" }}  \bar "'"  %1
  g''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  c'''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes c'''16 \unHideNotes \bar "'"  %1
  
  s16 <a'' g''>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  fis'''1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  des'1 \fermata  \bar "'"  %1
  s2. 
  s1
  bes'4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  bes''1 \startTrillSpan c'''  ^\markup { \box { "5''" }}  \bar "'"  %1
  c'''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  bes'2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes bes'16 \unHideNotes \bar "'"  %1
  
  s16 <bes'' c'''>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  a''1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  g''1 \fermata  \bar "'"  %1
  s2. 
  s1
  fis'''4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  des'1 \startTrillSpan bes'  ^\markup { \box { "5''" }}  \bar "'"  %1
  bes'1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  fis'''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes fis'''16 \unHideNotes \bar "'"  %1
  
  s16 <des' bes'>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  bes''1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  c'''1 \fermata  \bar "'"  %1
  s2. 
  s1
  a''4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  g''1 \startTrillSpan fis'''  ^\markup { \box { "5''" }}  \bar "'"  %1
  fis'''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  a''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes a''16 \unHideNotes \bar "'"  %1
  
  s16 <g'' fis'''>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  des'1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  bes'1 \fermata  \bar "'"  %1
  s2. 
  s1
  bes''4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  c'''1 \startTrillSpan a''  ^\markup { \box { "5''" }}  \bar "'"  %1
  a''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\override Tie #'details #'ratio = #0.01
\override Tie #'details #'height-limit = #0  
\override Tie #'Y-offset = #-0.6  
\override Tie #'thickness = #4 

\tieDotted 
  bes''2...~ \fermata  ^\markup { \box { "7''"  }} \hideNotes bes''16 \unHideNotes \bar "'"  %1
  
  s16 <c''' a''>2...  \longfermata  ^\markup { \box { "13''" }} \bar "'" %1
  \tieDotted 
  g''1~   ^\markup { \box { " 7'' "  }}  \bar "'"  %1
  fis'''1 \fermata  \bar "'"  %1
  s2. 
  s1
  des'4 \verylongfermata  ^\markup { \box { "18''" }} \bar "'" %1
  s1
  \pitchedTrill
  bes'1 \startTrillSpan bes''  ^\markup { \box { "5''" }}  \bar "'"  %1
  bes''1 \stopTrillSpan \fermata  ^\markup { \box { "9''" }}  \bar "||"  %1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
