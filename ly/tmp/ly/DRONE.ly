drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <bes' c ees, a, g,>2\laissezVibrer 
}drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <ees, a, g, bes' c>2\laissezVibrer 
}drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <g, bes' c ees, a,>2\laissezVibrer 
}drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <c ees, a, g, bes'>2\laissezVibrer 
}drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <a, g, bes' c ees,>2\laissezVibrer 
}drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <bes' c ees, a, g,>2\laissezVibrer 
}drone = \new Voice { 
 \change Staff = B  

 \proportionalStuff
 \override NoteHead #'color = #grey
 \override LaissezVibrerTie #'color = #grey
 \override Slur #'color = #grey
 \override Accidental #'color = #grey

 r2 <ees, a, g, bes' c>2\laissezVibrer 
}